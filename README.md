# 目次
    1. Python のインストール
    2. 実行環境の設定
        2.1. ipython の設定
            2.1.1. グラフ表示方法の設定
        2.2. Jupyter の設定
            2.2.1. 起動ブラウザの設定
            2.2.2. 起動ディレクトリの設定
            2.2.3. 起動トークンの非アクティブ化
        2.3. その他
            2.3.1. Windows のタスクバーにアイコンを設定する
    
# 1. Python のインストール
最新版の Python 3 を Anaconda からインストールする。

1. Web ブラウザから https://www.anaconda.com/download/ にアクセスする。
2. 自分のOS環境に対応する Python 3 のインストーラーを選択し、ダウンロードする。
3. インストーラーを起動し、Python 3 をインストールする。
4. デフォルトでは "%USERPROFILE%" の配下にインストールされる。
> <b>32-bit or 64-bit Python?</b><br>
Windows では 32-bit か 64-bit のインストーラーを選択することが出来ます。
システムが 32-bit プロセッサーの場合は、32-bit のインストーラーを選択します。
システムが 64-bit プロセッサーであれば、いずれのバージョンでも稼働します。
ただし、32-bit の方はメモリー消費が限定され、64-bit の方は集中的にアプリケーションを稼働させた際のパフォーマンスが高くなります。
どちらを選んだらよいのか分からなければ、まずは 64-bit から選択します。


# 2. 実行環境の設定
以下では、Jupyter で分析を開始するためのおすすめ初期設定を説明する。（任意）

## 2.1. ipython の設定
ipython のコンフィグファイルを作成し、設定する。
1. コマンドプロンプトを開き、以下のコマンドを実行し、"%USERPROFILE%/.ipython/profile_default/" を生成する。
>        ipython profile create

### 2.1.1. グラフ表示方法の設定
グラフ作成スクリプトを実行した際に、結果の画像を Jupyter のインラインで表示させることをデフォルトとする。
1. "ipython_config.py" を開き、以下のスクリプトに '%matplotlib inline' を追加する。
>        #c.InteractiveShellApp.exec_lines = ['%matplotlib inline']
2. 最初の "#" を削除（コメントイン）し、スクリプトをアクティブ化する。
>        c.InteractiveShellApp.exec_lines = ['%matplotlib inline']

## 2.2. Jupyter の設定
jupyter のコンフィグファイルを作成し、設定する。
1. コマンドプロンプトを開き、以下のコマンドを実行し、"%USERPROFILE%/.jupyter/jupyter_notebook_config.py" を生成する。
>        jupyter notebook --generate-config

### 2.2.1. 起動ブラウザの設定
Jupyter を起動させるデフォルトのブラウザを設定する。
1. "jupyter_notebook_config.py" を開き、任意の場所に以下のスクリプトを追加する。

FireFox にしたい場合：
>        import webbrowser
>        webbrowser.register('firefox', None, webbrowser.GenericBrowser(r'C:\Program Files (x86)\Mozilla Firefox\firefox.exe'))
>        c.NotebookApp.browser = 'firefox'

Chrome にしたい場合：
>        import webbrowser
>        webbrowser.register('chrome', None, webbrowser.GenericBrowser(r'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe'))
>        c.NotebookApp.browser = 'chrome'


### 2.2.2. 起動ディレクトリの設定
Jupyter を起動させるデフォルトの起動ディレクトリを設定する。
1. "jupyter_notebook_config.py" を開き、以下のスクリプトに起動したいディレクトリを追加する。
>        #c.NotebookApp.notebook_dir = r'C:\Users\＊ユーザー名＊\【起動ディレクトリ】'
2. 最初の "#" を削除（コメントイン）し、スクリプトをアクティブ化する。
>        c.NotebookApp.notebook_dir = r'C:\Users\＊ユーザー名＊\【起動ディレクトリ】'


### 2.2.3. 起動トークンの非アクティブ化
Jupyter 4.3 以降、標準では起動時にパスワード入力を求められる。個人の分析環境では特に必要ないので非アクティブ化する。
1. "jupyter_notebook_config.py" を開き、以下のスクリプトから \<generated\> を削除する。
>        #c.NotebookApp.token = ''
2. 最初の "#" を削除（コメントイン）し、スクリプトをアクティブ化する。
>        c.NotebookApp.token = ''

> 注意：Jupyterを公用する場合、認証の非アクティブ化は非推奨です。

## 2.3. その他
### 2.3.1. Windowsのタスクバーにアイコンを設定する
毎回コマンドプロンプトからコマンド実行で起動させるのは面倒なので、起動ボタンをタスクバーにピン留めしておく。
1. '%USERPROFILE%/Anaconda3/Scripts/' に移動する。
2. jupyter-notebook.exe を右クリックし、ショートカットファイルを作成する。
3. ショートカットアイコン画像を '%USERPROFILE%\Anaconda3\Menu\jupyter.ico' から取得し変更する。
4. 名前を "Jupyter" に変更する。
5. タスクバーにピン留めする。